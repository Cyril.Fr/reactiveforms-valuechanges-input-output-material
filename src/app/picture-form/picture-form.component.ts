import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-picture-form',
  templateUrl: './picture-form.component.html',
  styleUrls: ['./picture-form.component.css']
})
export class PictureFormComponent implements OnInit {

  public sizes: string[] = ['little', 'medium', 'tall'];
  public parentForm: FormGroup;

  @Output() formData: EventEmitter<any> = new EventEmitter<any>();

  constructor(private fb: FormBuilder) { }

  ngOnInit() {
    this.parentForm = this.fb.group({
      title: [],
      size: [],
      date: [],
      imgUrl: []
    });

    this.parentForm.valueChanges.subscribe(
      value => {
        console.log(value);
        this.formData.next(value);
      },
      error => { console.log(error); }
    );
  }

}
