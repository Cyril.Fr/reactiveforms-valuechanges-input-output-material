import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { PictureFormComponent } from './picture-form/picture-form.component';
import { SetupPictureComponent } from './setup-picture/setup-picture.component';
import { PictureComponent } from './picture/picture.component';
import { AppMaterialModule } from './app-material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    PictureFormComponent,
    SetupPictureComponent,
    PictureComponent
  ],
  imports: [
    BrowserModule,
    NoopAnimationsModule,
    AppMaterialModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
