import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SetupPictureComponent } from './setup-picture.component';

describe('SetupPictureComponent', () => {
  let component: SetupPictureComponent;
  let fixture: ComponentFixture<SetupPictureComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SetupPictureComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SetupPictureComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
