import { Component, OnInit } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Component({
  selector: 'app-setup-picture',
  templateUrl: './setup-picture.component.html',
  styleUrls: ['./setup-picture.component.css']
})
export class SetupPictureComponent implements OnInit {

  public formDataFromForm: BehaviorSubject<any> = new BehaviorSubject<any>(null);

  constructor() { }

  ngOnInit() {
  }

  onChangeForm(formData) {
    this.formDataFromForm.next(formData);
  }

}
